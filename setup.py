'''
                                  ESP Health
                          Lyme Disease Definition
                             Packaging Information
                                  
@author: Jeff Andre <jandre@commoninf.com>
@organization: commonwealth informatics http://www.commoninf.com
@contact: http://esphealth.org
@copyright: (c) 2022 Channing Laboratory
@license: LGPL 3.0 - http://www.gnu.org/licenses/lgpl-3.0.txt
'''

from setuptools import setup
from setuptools import find_packages

setup(
    name = 'esp-plugin-lyme',
    version = '1.8',
    author = 'Jeff Andre',
    author_email = 'jandre@commoninf.com',
    description = 'lyme disease definition module for ESP Health application',
    license = 'LGPLv3',
    keywords = 'lyme algorithm disease surveillance public health epidemiology',
    url = 'http://esphealth.org',
    packages = find_packages(exclude=['ez_setup']),
    install_requires = [
        ],
    entry_points = '''
        [esphealth]
        disease_definitions = lyme:disease_definitions
        event_heuristics = lyme:event_heuristics
    '''
    )
