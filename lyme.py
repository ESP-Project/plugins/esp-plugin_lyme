'''
                                  ESP Health
                         Notifiable Diseases Framework
                           Lyme Case Generator


@author: Jeff Andre <jandre@commoninf.com>
@organization: commonwealth informatics http://www.commoninf.com
@contact: http://www.esphealth.org
@copyright: (c) 2022 Commonwealth Informatics, Inc.
@license: LGPL
'''

# In most instances it is preferable to use relativedelta for date math.
# However when date math must be included inside an ORM query, and thus will
# be converted into SQL, only timedelta is supported.
from datetime import timedelta
from dateutil.relativedelta import relativedelta

from django.db.models import F

from ESP.utils import log
from ESP.hef.base import Event
from ESP.hef.base import PrescriptionHeuristic
from ESP.hef.base import LabResultPositiveHeuristic

from ESP.hef.base import DiagnosisHeuristic
from ESP.hef.base import Dx_CodeQuery
from ESP.nodis.base import DiseaseDefinition
from ESP.static.models import DrugSynonym



class Lyme(DiseaseDefinition):
    '''
    Lyme
    '''

    conditions = ['lyme']

    uri = 'urn:x-esphealth:disease:channing:lyme:v1'

    short_name = 'lyme'

    test_name_search_strings = [
            'lyme',
            'burg',
            'borr',
            'mttt',
            'zeus',
            ]

    timespan_heuristics = []

    recurrence = 365

    @property
    def event_heuristics(self):
        heuristic_list = []
        #
        # Diagnosis Codes
        #
        heuristic_list.append( DiagnosisHeuristic(
            name = 'lyme',
            dx_code_queries = [
                Dx_CodeQuery(starts_with='088.81', type='icd9'),
                Dx_CodeQuery(starts_with='A69.20', type='icd10'),
                Dx_CodeQuery(starts_with='A69.21', type='icd10'),
                Dx_CodeQuery(starts_with='A69.22', type='icd10'),
                Dx_CodeQuery(starts_with='A69.23', type='icd10'),
                Dx_CodeQuery(starts_with='A69.29', type='icd10'),
                ]
            ))
        #
        # Prescriptions
        #
        heuristic_list.append( PrescriptionHeuristic(
            name = 'lyme_doxycycline',
            drugs = DrugSynonym.generics_plus_synonyms(['doxycycline', ]),
            min_quantity=14,  # Need 14 pills for 7 days
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'lyme_other_antibiotics',
            drugs =  DrugSynonym.generics_plus_synonyms(['Amoxicillin',
                                                         'Cefuroxime',
                                                         'Ceftriaxone',
                                                         'Cefotaxime',
                                                         'Azithromycin',
                                                         'Tetracycline',
                                                         'Cefuroxime axetil']),
            min_quantity=14,  # Need 14 pills for 7 days
            ))

        #
        # Lab Results
        #
        heuristic_list.append( LabResultPositiveHeuristic(
            test_name = 'lyme_mttta_conf',
            ))
        heuristic_list.append( LabResultPositiveHeuristic(
            test_name = 'lyme_wb',
            ))
        heuristic_list.append( LabResultPositiveHeuristic(
            test_name = 'lyme_pcr',
            ))
        heuristic_list.append( LabResultPositiveHeuristic(
            test_name = 'lyme_ab_non_confirm',
            ))

        return heuristic_list

    def generate(self):
        log.info('Generating cases of %s' % self.short_name)
        self.criteria = ''
        #
        # Criteria Set #1
        # diagnosis and meds within 14 days
        #
        dx_event_names = ['dx:lyme']
        rx_event_names = [
            'rx:lyme_doxycycline',
            'rx:lyme_other_antibiotics',
            ]
        dxrx_event_qs = Event.objects.filter(
            name__in = dx_event_names,
            patient__event__name__in = rx_event_names,
            patient__event__date__gte = (F('date') - timedelta(days=14) ),
            patient__event__date__lte = (F('date') + timedelta(days=14)  ),
            )

        # Criteria #2
        # Positive Lyme Western Blot
        #
        wb_event_names = [
            'lx:lyme_wb:positive', ]

        wb_event_qs = Event.objects.filter(
            name__in =  wb_event_names,
            )

        #
        # Criteria #3
        # Positive Lyme PCR
        #
        pcr_event_names = [
            'lx:lyme_pcr:positive', ]

        pcr_event_qs = Event.objects.filter(
            name__in =  pcr_event_names,
            )

        #
        # Criteria Set #4
        # Two Positive EIA from an FDA-approved modified two-tiered testing algorithm (MTTTA)
        # An acceptable implementation of this criterion is to look for the positive confirmatory MTTTA EIA test alone
        #
        mttta_event_names = [
            'lx:lyme_mttta_conf:positive',]

        mttta_event_qs = Event.objects.filter(
            name__in = mttta_event_names,
            )

        #
        # Combined Criteria
        #
        combined_criteria_qs = dxrx_event_qs | wb_event_qs | pcr_event_qs | mttta_event_qs
        combined_criteria_qs = combined_criteria_qs.exclude(case__condition=self.conditions[0])
        combined_criteria_qs = combined_criteria_qs.order_by('date')

        all_event_names = dx_event_names + rx_event_names + wb_event_names + pcr_event_names + mttta_event_names
        counter = 0

        for this_event in combined_criteria_qs:

            new_criteria = ''
            if this_event.name in dx_event_names:
                new_criteria =  'Criteria #1 Lyme DxCode and antibiotics within 14 days'
            elif this_event.name in wb_event_names:
                new_criteria = 'Criteria #2 Positive Lyme Western Blot'
            elif this_event.name in pcr_event_names:
                new_criteria = 'Criteria #3 Positive Lyme PCR'
            elif this_event.name in mttta_event_names:
                new_criteria += 'Criteria #4 Two Positive EIA from FDA-approved MTTTA or MTTA Confirmatory'

            if new_criteria:
                relevant_events = Event.objects.filter(patient=this_event.patient, name__in=all_event_names)
                created, new_case = self._create_case_from_event_obj(
                    condition=self.conditions[0],
                    criteria=new_criteria,
                    recurrence_interval=self.recurrence,
                    event_obj=this_event,
                    relevant_event_qs=relevant_events
                )

            if created:
                log.info('Created new lyme case: %s' % new_case)
                counter += 1

        log.debug('Generated %s new cases of lyme' % counter)

        return counter # Count of new cases


#-------------------------------------------------------------------------------
#
# Packaging
#
#-------------------------------------------------------------------------------

lyme_definition = Lyme()

def event_heuristics():
    return lyme_definition.event_heuristics

def disease_definitions():
    return [lyme_definition]
	
